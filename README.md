# Compiler flags for NEON on AArch64

### Flags to generally use:
* `aarch64-linux-gnu-g++` - Compiler to use (in this case C++ compiler)
* `-std=c++11` - Standard to use
* `-mcpu=cortex-a53` - CPU to optimize for
* `-ftree-vectorize` - Enable the vectorizing option in the compiler
* `-ffast-math` - The unsafe-math flag is needed to use NEON, fast-math contains this plus some more
* `-mfix-cortex-a53-835769` - Enable the fix for errata 835769
* `-lm -O2` - Some standard flags. O2 is enough, but O3 can be used as well

### Debug flags:
* `-fopt-info-vec-all` - Optional debug output of the vectorizer (to stderr)
* `-fdump-tree-vect-details` - Optional debug output of the vectorizer dumped to a file
* `-S` - Produce assembly output for inspection

### Things to think about when writing vectorizing code:
* Use `__restrict` whereever possible (read up on it, it is important!), tells the compilers that pointers are not aliasing
* Make operations in parallel of 128-bit (16 x 8-bit / 8 x 16-bit / 4 x 32-bit / ...), indicate this in the loop condition
* When creating a new function that should use vecortization, make a minimal example and inspect the assembler + debug output, it has a lot of hints
* See `example.cpp` / `Makefile` for example usage
