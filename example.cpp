
#include <cstdint>
#include <cmath>

void vector16x8_add(int32_t *__restrict out,
                    const int8_t* __restrict p,
                    const int8_t* __restrict q,
                    const unsigned int n)
{
    unsigned int i;
    int32_t s = 0;

    for (i = 0; i < (n & ~15); i++)
    {
        s += std::abs(p[i] - q[i]);
    }

    *out = s;
}
