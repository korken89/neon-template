all:
	aarch64-linux-gnu-g++ -std=c++11 -mcpu=cortex-a53 -ftree-vectorize -ffast-math -mfix-cortex-a53-835769 -lm -O2 -fopt-info-vec-all -fdump-tree-vect-details example.cpp -S 2> gccdmp

clean:
	rm gccdmp
	rm *.s *.vect

